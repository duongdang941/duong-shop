<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*BACKEND ROUTES*/

Route::get('/admin', [\App\Http\Controllers\Backend\AuthController::class, 'index'])->name('admin.login');
Route::post('admin/login', [\App\Http\Controllers\Backend\AuthController::class, 'login'])->name('auth.login');
Route::get('/get-districts', [\App\Http\Controllers\Ajax\LocationController::class, 'getDistricts'])->name('ajax.get.districts');
Route::get('/get-wards/', [\App\Http\Controllers\Ajax\LocationController::class, 'getWards'])->name('ajax.get.wards');

Route::group(['prefix'=>'/admin','middleware'=>['admin']],function(){
    Route::group(['prefix'=>'/user'],function(){
        Route::get('/', [\App\Http\Controllers\Backend\UserController::class, 'index'])->name('admin.user');
        Route::get('/create', [\App\Http\Controllers\Backend\UserController::class, 'create'])->name('admin.user.create');
        Route::post('/store', [\App\Http\Controllers\Backend\UserController::class, 'store'])->name('admin.user.store');
        Route::get('/edit/{id}', [\App\Http\Controllers\Backend\UserController::class, 'edit'])->name('admin.user.edit');
        Route::post('/update/{id}', [\App\Http\Controllers\Backend\UserController::class, 'update'])->name('admin.user.update');
        Route::delete('/destroy/{id}', [\App\Http\Controllers\Backend\UserController::class, 'destroy'])->name('admin.user.destroy');
        Route::post('/change-status', [\App\Http\Controllers\Backend\UserController::class, 'changeStatus'])->name('admin.user.change.status');
    });

    Route::group(['prefix'=>'/role'],function(){
        Route::get('/', [\App\Http\Controllers\Backend\RoleController::class, 'index'])->name('admin.role');
        Route::get('/create', [\App\Http\Controllers\Backend\RoleController::class, 'create'])->name('admin.role.create');
        Route::post('/store', [\App\Http\Controllers\Backend\RoleController::class, 'store'])->name('admin.role.store');
        Route::get('/edit/{id}', [\App\Http\Controllers\Backend\RoleController::class, 'edit'])->name('admin.role.edit');
        Route::post('/update/{id}', [\App\Http\Controllers\Backend\RoleController::class, 'update'])->name('admin.role.update');
        Route::delete('/destroy/{id}', [\App\Http\Controllers\Backend\RoleController::class, 'destroy'])->name('admin.role.destroy');
        Route::post('/change-status', [\App\Http\Controllers\Backend\RoleController::class, 'changeStatus'])->name('admin.role.change.status');
    });



    Route::get('/logout', [\App\Http\Controllers\Backend\AuthController::class, 'logout'])->name('auth.logout');
    Route::get('/dashboard', [\App\Http\Controllers\Backend\DashboardController::class, 'index'])->name('admin.dashboard');
    Route::post('/ajax/dashboard/change-status', [\App\Http\Controllers\Ajax\DashboardController::class, 'changeStatus'])->name('ajax.admin.dashboard.change.status');
    Route::post('/ajax/dashboard/change-all-status', [\App\Http\Controllers\Ajax\DashboardController::class, 'changeStatusAll'])->name('ajax.admin.dashboard.change.status.all');
});
