// webpack.mix.js

let mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/backend/location.js', 'public/js')
    .js('resources/js/backend/custom.js', 'public/backend/js')
    .postCss('resources/css/app.css', 'public/css', [
        require("tailwindcss"),
    ])
    .postCss('resources/css/style.css', 'public/css')
    .postCss('resources/css/backend/custom.css', 'public/backend')
    .sass('resources/sass/app.scss', 'public/sass')
    .version()
    .sourceMaps();
