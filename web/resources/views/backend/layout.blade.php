<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('title')
    @include('backend.component.head')
    @stack('styles')
</head>

<body>
<div id="wrapper">
    @include('backend.component.sidebar')

    <div id="page-wrapper" class="gray-bg">
        @include('backend.component.header')
        @yield('main-content')
        @include('backend.component.footer')
    </div>

</div>

<!-- Mainly scripts -->
@include('backend.component.script')
@stack('scripts')
</body>
</html>
