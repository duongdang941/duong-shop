@extends('backend.layout')
@section('title', env('APP_NAME') . ' || User')
@push('styles')
    <link href="{{asset('backend/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
@endpush
@section('main-content')
    @include('backend.component.breadcrumb', ['title' => config('apps.user.create.title')])
    <form action="{{route('admin.user.update', $user->id)}}" method="POST">
        @csrf
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-5">
                    <div class="panel-header">
                        <div class="panel-title">Thong tin chung</div>
                        <div class="panel-description">Nhap thong tin cua nguoi su dung</div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Thong tin chung</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-row mb15">
                                        <label for="name" class="control-label text-right">Ho va ten
                                            <span class="text-danger">(*)</span>
                                        </label>
                                        <input
                                            class="form-control"
                                            type="text"
                                            name="name"
                                            value="{{old('name', $user->name ?? '')}}"
                                            autocomplete="off"
                                            placeholder="">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-row mb15">
                                        <label for="" class="control-label text-right">Email
                                            <span class="text-danger">(*)</span>
                                        </label>
                                        <input
                                            class="form-control"
                                            type="email"
                                            name="email"
                                            value="{{old('email', $user->email ?? '')}}"
                                            autocomplete="off"
                                            placeholder="">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb15" id="data_1">
                                        <label class="control-label text-right">Ngay sinh
                                            <span class="text-danger">(*)</span>
                                        </label>
                                        <div class="input-group date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control" value="{{old('birthday', \Illuminate\Support\Carbon::parse($user->birthday)->format('d/m/Y') ?? '') ?? now()->format('d/m/Y') }}" name="birthday">
                                        </div>
                                        @error('birthday')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-row mb15">
                                        <label for="group" class="control-label text-right">Nhom
                                        </label>
                                        <select class="form-control select2" name="group">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                        </select>
                                        @error('group')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-row mb15">
                                        <label for="avatar" class="control-label text-right">Anh dai dien
                                        </label>
                                        <input
                                            class="form-control"
                                            type="file"
                                            name="avatar"
                                            autocomplete="off"
                                            placeholder="">
                                    </div>

                                    @error('avatar')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-lg-5">
                    <div class="panel-header">
                        <div class="panel-title">Thong tin lien he</div>
                        <div class="panel-description">Nhap thong tin lien he cua nguoi su dung</div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Thong tin lien he</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row mb15">
                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <label for="provinces" class="control-label text-right">Tinh/Thanh pho</label>
                                        <select class="form-control select2 provinces" name="province_id" data-target="districts">
                                            <option value="0">[Chon thanh pho]</option>
                                            @if(!empty($location) && !empty($location['provinces']))
                                                @foreach($location['provinces'] as $province)
                                                    <option @if(old('province_id') == $province->code) selected @endif value="{{ $province->code }}">{{ $province->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                        @error('province')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <label for="districts" class="control-label text-right">Quan/Huyen</label>
                                        <select class="form-control select2 districts" name="district_id" data-target="wards">
                                            <option value="0">[Chon Quan/Huyen]</option>
                                        </select>
                                    </div>

                                    @error('district')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <label for="wards" class="control-label text-right">Phuong/Xa</label>
                                        <select class="form-control select2 wards" name="ward_id">
                                            <option value="0">[Chon Phuong/Xa]</option>
                                        </select>
                                    </div>

                                    @error('wards')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <label for="phone" class="control-label text-right">So dien thoai</label>
                                        <input
                                            class="form-control"
                                            type="number"
                                            name="phone"
                                            value="{{old('phone', $user->phone)}}"
                                            autocomplete="off"
                                            placeholder="">
                                    </div>

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <label for="address" class="control-label text-right">Dia chi</label>
                                        <input
                                            class="form-control"
                                            type="text"
                                            name="address"
                                            value="{{old('address', $user->address ?? '')}}"
                                            autocomplete="off"
                                            placeholder="">
                                    </div>

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <label for="description" class="control-label text-right">Ghi chu</label>
                                        <input
                                            class="form-control"
                                            type="text"
                                            name="description"
                                            value="{{old('description', $user->description ?? '')}}"
                                            autocomplete="off"
                                            placeholder="">
                                    </div>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-right mb15">
                <button type="submit" class="btn btn-primary">Luu</button>
            </div>
        </div>
    </form>

@endsection
@push('scripts')
    <script>
        let getDistrictsUrl = '{{route('ajax.get.districts')}}';
        let getWardsUrl = '{{route('ajax.get.wards')}}'
        var province_id = '{{ (isset($user->province_id)) ? $user->province_id : old('province_id') }}'
        var district_id = '{{ (isset($user->district_id)) ? $user->district_id : old('district_id') }}'
        var ward_id = '{{ (isset($user->ward_id)) ? $user->ward_id : old('ward_id') }}'
    </script>
    <script src="{{asset("js/location.js")}}"></script>
    <!-- Data picker -->
    <script src="{{asset('backend/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
    {{--    Select2--}}
    <script src="{{asset("backend/js/plugins/select2/select2.full.min.js")}}"></script>
    <script>
        $(document).ready(function(){
            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $('.select2').select2();
        });
    </script>
@endpush
