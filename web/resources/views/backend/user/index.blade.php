@extends('backend.layout')
@section('title', env('APP_NAME') . ' || User')
@push('styles')
    <link href="{{asset('backend/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
@endpush
@section('main-content')
@include('backend.component.breadcrumb', ['title' => config('apps.user')['index']['title']])
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{{config('apps.user')['index']['table']}}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="change-status-all" data-status="active" data-model="User" data-field="status">Active All</a>
                                </li>
                                <li><a href="#" class="change-status-all" data-status="inactive" data-model="User" data-field="status">Inactive All</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content">
                        @include('backend.user.component.filter')
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkAll" value=""></th>
                                    <th>Ho ten</th>
                                    <th style="width: 90px">Avatar</th>
                                    <th>Email</th>
                                    <th>So dien thoai</th>
                                    <th>Dia chi</th>
                                    <th>Trang thai</th>
                                    <th>Thao tac</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($users))
                                    @foreach($users as $user)
                                        <tr class="gradeX">
                                            <td><input type="checkbox" class="checkbox-item" value="{{$user->id}}"></td>
                                            <td>
                                                <div class="name">{{$user->name ?? ''}}</div>
                                            </td>
                                            <td>
                                                <div>
                                                    <img style="width: 100%; height: 100%; object-fit: cover" src="https://i.pinimg.com/736x/24/21/85/242185eaef43192fc3f9646932fe3b46.jpg" alt="">
                                                </div>
                                            </td>
                                            <td>
                                                <span>{{$user->email ?? ''}}</span>
                                            </td>
                                            <td>
                                                <span>{{$user->phone ?? ''}}</span>
                                            </td>
                                            <td>
                                                <div class="text-center"> {{$user->address ?? ''}}</div>
                                            </td>
                                            <td class="text-center">
                                                <input type="checkbox" data-id="{{$user->id}}" class="js-switch status"
                                                       data-field="status" data-model="User" value="{{$user->status}}"
                                                    {{$user->status == 'active' ? 'checked' : ''}} />
                                            </td>
                                            <td class="text-center">
                                                <a href="{{route('admin.user.edit', $user->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                                <button onclick="deleteUser({{$user->id}})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Ho ten</th>
                                    <th>Avatar</th>
                                    <th>Email</th>
                                    <th>So dien thoai</th>
                                    <th>Dia chi</th>
                                    <th>Trang thai</th>
                                    <th>Thao tac</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="pagination-right">
                            {{ $users->links('pagination::bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <!-- Select2 -->
    <script src="{{asset("backend/js/plugins/select2/select2.full.min.js")}}"></script>
    <script>
        var deleteUserUrl = '{{route('admin.user.destroy','')}}'
        var changeUserStatusUrl = '{{route('ajax.admin.dashboard.change.status')}}'
        var changeAllUserStatusUrl = '{{route('ajax.admin.dashboard.change.status.all')}}'
    </script>
    <script>
        $(document).ready(function(){
            checkBox();
            changeStatus();
            changeStatusAll();
            option();
        });

        function deleteUser(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (result.value) {
                    setupAjax();
                    sendAjax(deleteUserUrl + '/' + id, {}, 'DELETE').done(function (response) {
                        if (response.success) {
                            toastr.success(response.message)
                            location.reload();
                        } else {
                            toastr.error(response.message)
                        }
                    })
                }
            });
        }

        function setupAjax() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        function sendAjax(url, data = {}, method = 'GET') {
            return $.ajax({
                url: url,
                method: method,
                data: data,
            });
        }

        function checkBox() {
            $('.checkAll').click(function (){
                let isChecked = $(this).prop('checked');

                if(isChecked) {
                    $('.checkbox-item').each(function (){
                        $(this).prop('checked', true)
                        $(this).closest('tr').addClass('active')
                    })
                } else {
                    $('.checkbox-item').each(function (){
                        $(this).prop('checked', false)
                        $(this).closest('tr').removeClass('active')
                    })
                }
            });

            $('.checkbox-item').click(function (){
                let isChecked = $(this).prop('checked');
                allChecked();

                if(isChecked) {
                    $(this).closest('tr').addClass('active')
                } else {
                    $(this).closest('tr').removeClass('active')
                    $('.checkAll').prop('checked', false)
                }
            })

            function allChecked() {
                if($('.checkbox-item:checked').length === $('.checkbox-item').length){
                    $('.checkAll').prop('checked', true)
                }
            }
        }

        function changeStatus() {
            $('.status').change(function (e) {
                let userId = $(this).attr('data-id') ?? 0;
                let status = 'active';
                let model  = $(this).attr('data-model')
                let field  = $(this).attr('data-field')
                let data;

                if(!$(this).is(':checked')) {
                    status = 'inactive'
                }

                data = ({'id' : userId, 'status' : status, 'model': model, 'field': field });

                setupAjax();
                sendAjax(changeUserStatusUrl, data, 'POST').done(function (response) {
                    if (response.success) {
                        toastr.success(response.message)
                    } else {
                        toastr.error(response.message)
                    }
                })
                e.preventDefault();
            })
        }

        function changeStatusAll() {
            $('.change-status-all').click(function (e){
                let id = [];
                let status = $(this).attr('data-status')
                let model  = $(this).attr('data-model')
                let field  = $(this).attr('data-field')
                let data;

                $('.checkbox-item:checked').each(function (){
                    id.push($(this).val());
                })

                data = ({'ids' : id, 'status' : status, 'model': model, 'field': field });

                setupAjax();
                sendAjax(changeAllUserStatusUrl, data, 'POST').done(function (response) {
                    if (response.success) {
                        toastr.success(response.message)
                        location.reload();
                    } else {
                        toastr.error(response.message)
                    }
                })
                e.preventDefault();

            })
        }

        function option() {
            $(".select2_demo_2").select2();

            $('.js-switch').each(function(){
                new Switchery(this, { color: '#1AB394' });
            })
        }


    </script>
@endpush
