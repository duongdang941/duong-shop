<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg"/>
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong
                                        class="font-bold">David Williams</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b
                                        class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{request()->routeIs('admin.dashboard') ? 'active' : ''}}">
                <a href="{{route('admin.dashboard')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
            </li>

            <li class="{{request()->routeIs('admin.user', 'admin.user.*', 'admin.role', 'admin.role.*') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-user"></i> <span class="nav-label">QL Thanh Vien</span><span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{request()->routeIs('admin.user', 'admin.user.*') ? 'active' : ''}}">
                        <a href="{{route('admin.user')}}">QL Thanh Vien</a>
                    </li>
                    <li class="{{request()->routeIs('admin.role.*', 'admin.role') ? 'active' : ''}}">
                        <a href="{{route('admin.role')}}">QL Nhom Thanh Vien</a>
                    </li>
                </ul>
            </li>

        </ul>

    </div>
</nav>
