<!DOCTYPE html>
<html>

<head>
    <title>{{ env('APP_NAME') }} || Login Page</title>
    @include('backend.component.head')

</head>

<body class="gray-bg">

<div class="loginColumns animated fadeInDown">
    <div class="row login-form">
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" role="form" action="{{route('auth.login')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Email"
                               value="{{old('email')}}">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                    <a href="#">
                        <small>Forgot password?</small>
                    </a>

                </form>
                <p class="m-t">
                    <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                </p>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('backend/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('backend/js/bootstrap.min.js')}}"></script>

</body>

</html>
