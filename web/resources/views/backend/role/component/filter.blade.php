<form action="{{route('admin.user')}}" method="GET">
    <div class="filter-wrapper">
        <div class="flex justify-between">
            <div class="perpage">
                @php
                    $perpage = request('per_page') ?: old('per_page');
                @endphp
                <div class="flex">
                    <select name="per_page" class="input-sm perpage filter h-32">
                        @for($i = 10; $i<= 100 ; $i+=10)
                            <option {{ ($perpage == $i)  ? 'selected' : '' }}  value="{{ $i }}">{{ $i }} bản ghi</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="action flex">
                <div class="flex w-full">
                    @php
                        $oldStatus = request('status') ?: old('status');
                        $statuses = ['active' => 'Active', 'inactive' => 'Inactive']
                    @endphp
                    <select name="status" class="setupSelect2 mr10 h-32">
                        <option value="0">Select status</option>
                        @foreach($statuses as $key => $status)
                            <option {{ ($oldStatus == $key)  ? 'selected' : '' }}  value="{{ $key }}">{{$status}}</option>
                        @endforeach
                    </select>
                    <select name="group" class="setupSelect2 mr10 h-32">
                        <option value="0" selected="selected">Chọn Nhóm Thành Viên</option>
                        <option value="1">Nhom Admin</option>
                        <option value="1">Nhom User</option>
                    </select>

                    <div class="flex w-full search">
                        <input
                            class="w-full"
                            type="text"
                            name="keyword"
                            value="{{ request('keyword') ?: old('keyword') }}"
                            placeholder="Nhập Từ khóa bạn muốn tìm kiếm..."
                        >
                        <span class="">
                           <button type="submit" name="search" value="search" class="btn btn-primary btn-sm h-32 mb0">Tìm Kiếm</button>
                       </span>
                    </div>
                </div>
                <a href="{{ route('admin.role.create') }}" class="btn btn-danger mb0 h-32"><i class="fa fa-plus mr5"></i>Thêm nhom moi</a>
            </div>
        </div>
    </div>
</form>
