@extends('backend.layout')
@section('title', env('APP_NAME') . ' || role')
@push('styles')
    <link href="{{asset('backend/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
@endpush
@section('main-content')
    @include('backend.component.breadcrumb', ['title' => config('apps.role.create.title')])
    <form action="{{route('admin.role.update', $role->id)}}" method="POST">
        @csrf
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-5">
                    <div class="panel-header">
                        <div class="panel-title">Thong tin chung</div>
                        <div class="panel-description">Nhap thong tin cua nhom</div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Thong tin chung</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-row mb15">
                                        <label for="name" class="control-label text-right">Ho va ten
                                            <span class="text-danger">(*)</span>
                                        </label>
                                        <input
                                            class="form-control"
                                            type="text"
                                            name="name"
                                            value="{{old('name', $role->name ?? '')}}"
                                            autocomplete="off"
                                            placeholder="">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-row mb15">
                                        <label for="" class="control-label text-right">Mo ta
                                            <span class="text-danger">(*)</span>
                                        </label>
                                        <input
                                            class="form-control"
                                            type="text"
                                            name="description"
                                            value="{{old('email', $role->description ?? '')}}"
                                            autocomplete="off"
                                            placeholder="">
                                        @error('description')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-right mb15">
                <button type="submit" class="btn btn-primary">Luu</button>
            </div>
        </div>
    </form>

@endsection
@push('scripts')
    <!-- Data picker -->
    <script src="{{asset('backend/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
    {{--    Select2--}}
    <script src="{{asset("backend/js/plugins/select2/select2.full.min.js")}}"></script>
    <script>
        $(document).ready(function(){
            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $('.select2').select2();
        });
    </script>
@endpush
