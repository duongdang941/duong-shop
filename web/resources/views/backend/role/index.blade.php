@extends('backend.layout')
@section('title', env('APP_NAME') . ' || role')
@push('styles')
    <link href="{{asset('backend/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
@endpush
@section('main-content')
@include('backend.component.breadcrumb', ['title' => config('apps.role')['index']['title']])
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{{config('apps.role')['index']['table']}}</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-role">
                                <li><a href="#" class="change-status-all" data-status="active" data-model="role" data-field="status">Active All</a>
                                </li>
                                <li><a href="#" class="change-status-all" data-status="inactive" data-model="role" data-field="status">Inactive All</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content">
                        @include('backend.role.component.filter')
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="checkAll" value=""></th>
                                    <th>Ten</th>
                                    <th>Description</th>
                                    <th>Thao tac</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($roles))
                                    @foreach($roles as $role)
                                        <tr class="gradeX">
                                            <td><input type="checkbox" class="checkbox-item" value="{{$role->id}}"></td>
                                            <td>
                                                <div class="name">{{$role->name ?? ''}}</div>
                                            </td>

                                            <td>
                                                <div class="name">{{$role->description ?? ''}}</div>
                                            </td>

                                            <td class="text-center">
                                                <a href="{{route('admin.role.edit', $role->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                                <button onclick="deleterole({{$role->id}})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>ten</th>
                                    <th>Description</th>
                                    <th>Thao tac</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="pagination-right">
                            {{ $roles->links('pagination::bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <!-- Select2 -->
    <script src="{{asset("backend/js/plugins/select2/select2.full.min.js")}}"></script>
    <script>
        var deleteroleUrl = '{{route('admin.role.destroy','')}}'
        {{--var changeroleStatusUrl = '{{route('ajax.admin.dashboard.change.status')}}'--}}
        {{--var changeAllroleStatusUrl = '{{route('ajax.admin.dashboard.change.status.all')}}'--}}
    </script>
    <script>
        $(document).ready(function(){
            checkBox();
            changeStatus();
            changeStatusAll();
            option();
        });

        function deleterole(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (result.value) {
                    setupAjax();
                    sendAjax(deleteroleUrl + '/' + id, {}, 'DELETE').done(function (response) {
                        if (response.success) {
                            toastr.success(response.message)
                            location.reload();
                        } else {
                            toastr.error(response.message)
                        }
                    })
                }
            });
        }

        function setupAjax() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        function sendAjax(url, data = {}, method = 'GET') {
            return $.ajax({
                url: url,
                method: method,
                data: data,
            });
        }

        function checkBox() {
            $('.checkAll').click(function (){
                let isChecked = $(this).prop('checked');

                if(isChecked) {
                    $('.checkbox-item').each(function (){
                        $(this).prop('checked', true)
                        $(this).closest('tr').addClass('active')
                    })
                } else {
                    $('.checkbox-item').each(function (){
                        $(this).prop('checked', false)
                        $(this).closest('tr').removeClass('active')
                    })
                }
            });

            $('.checkbox-item').click(function (){
                let isChecked = $(this).prop('checked');
                allChecked();

                if(isChecked) {
                    $(this).closest('tr').addClass('active')
                } else {
                    $(this).closest('tr').removeClass('active')
                    $('.checkAll').prop('checked', false)
                }
            })

            function allChecked() {
                if($('.checkbox-item:checked').length === $('.checkbox-item').length){
                    $('.checkAll').prop('checked', true)
                }
            }
        }

        function changeStatus() {
            $('.status').change(function (e) {
                let roleId = $(this).attr('data-id') ?? 0;
                let status = 'active';
                let model  = $(this).attr('data-model')
                let field  = $(this).attr('data-field')
                let data;

                if(!$(this).is(':checked')) {
                    status = 'inactive'
                }

                data = ({'id' : roleId, 'status' : status, 'model': model, 'field': field });

                setupAjax();
                sendAjax(changeroleStatusUrl, data, 'POST').done(function (response) {
                    if (response.success) {
                        toastr.success(response.message)
                    } else {
                        toastr.error(response.message)
                    }
                })
                e.preventDefault();
            })
        }

        function changeStatusAll() {
            $('.change-status-all').click(function (e){
                let id = [];
                let status = $(this).attr('data-status')
                let model  = $(this).attr('data-model')
                let field  = $(this).attr('data-field')
                let data;

                $('.checkbox-item:checked').each(function (){
                    id.push($(this).val());
                })

                data = ({'ids' : id, 'status' : status, 'model': model, 'field': field });

                setupAjax();
                sendAjax(changeAllroleStatusUrl, data, 'POST').done(function (response) {
                    if (response.success) {
                        toastr.success(response.message)
                        location.reload();
                    } else {
                        toastr.error(response.message)
                    }
                })
                e.preventDefault();

            })
        }

        function option() {
            $(".select2_demo_2").select2();

            $('.js-switch').each(function(){
                new Switchery(this, { color: '#1AB394' });
            })
        }


    </script>
@endpush
