(function($) {
  "use strict";

  function getLocation() {
    $('.provinces').change(function () {
      let province_id = $(this).val();
      let data = {'province_id': province_id};
      console.log(province_id)
      sendAjax($(this), getDistrictsUrl, data)
    })

    $('.districts').change(function (){
      let districts_id = $(this).val();
      let data = {'district_id': districts_id};

      sendAjax($(this), getWardsUrl, data)
    })
  }

  function loadCity() {
    if(province_id != ''){
      $(".provinces").val( province_id ).trigger('change');
    }
  }

  function sendAjax(el, url, data, method = 'GET',) {
    let target = el.attr('data-target')

    $.ajax({
      url: url,
      method: method,
      dataType: 'json',
      data: data,
      success: function(response) {
        $('.' + target).html(response.html)

        if(district_id != '' && target == 'districts'){
          $(".districts").val( district_id ).trigger('change');
        }

        if(ward_id != '' && target == 'wards'){
          $('.wards').val(ward_id).trigger('change');
        }
      },
      error: function(xhr, status, error) {
        console.error(error);
        // Xử lý lỗi (nếu có)
      }
    });
  }

  $(document).ready(function(){
    getLocation();
    loadCity();
  });

})(jQuery);
