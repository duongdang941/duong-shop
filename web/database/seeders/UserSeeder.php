<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'Dang Trung Duong',
            'email' => 'duongdang941@gmail.com',
            'password' => Hash::make('Duong12345')
        ]);
        User::factory()->count(200)->create();
    }
}
