<?php

namespace App\Services\Interfaces;

/**
 * Interface UserRepositoryInterface
 * @package App\Services\Interfaces
 */
interface UserServiceInterface
{
    public function paginate($request);

    public function changeStatus($post = []);

    public function changeStatusAll($post = []);
}
