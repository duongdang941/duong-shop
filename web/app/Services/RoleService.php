<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use App\Services\Interfaces\RoleServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use App\Repositories\UserRepository as UserRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

/**
 * Class UserService
 * @package App\Services
 */
class RoleService implements RoleServiceInterface
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function paginate($request) {
        $condition = [];
        $condition['keyword'] = addslashes($request->input('keyword'));
        $condition['status'] = $request->input('status');
        $perPage = $request->input('per_page') ?? 10;
        $path = route('admin.role');
        return $this->roleRepository->pagination((int)$perPage, ['*'], $condition, [], $path);
    }

    public function create($request) {
        DB::beginTransaction();
        try{
            $payload = $request->except(['_token','send']);
            $this->roleRepository->create($payload);

            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function update($request, $id) {
        DB::beginTransaction();
        try{
            $payload = $request->except(['_token','send']);
            $this->roleRepository->update($id, $payload);

            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function delete($id) {
        DB::beginTransaction();
        try{
            $this->roleRepository->delete($id);
            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }

//
//    public function changeStatus($post = []) {
//
//        DB::beginTransaction();
//        try{
//            $payload[$post['field']] = $post['status'];
//            $id = $post['id'] ?? '';
//            $this->userRepository->update($id, $payload);
//
//            DB::commit();
//            return true;
//
//        }catch(\Exception $e ){
//            DB::rollBack();
//            Log::error($e->getMessage());
//            return false;
//        }
//    }
//
//    public function changeStatusAll($post = []) {
//        DB::beginTransaction();
//        try{
//            $payload[$post['field']] = $post['status'];
//            $ids = $post['ids'] ?? [];
//            $this->userRepository->updateByWhereIn('id', $ids, $payload);
//
//            DB::commit();
//            return true;
//
//        }catch(\Exception $e ){
//            DB::rollBack();
//            Log::error($e->getMessage());
//            return false;
//        }
//    }
}
