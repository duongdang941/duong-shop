<?php

namespace App\Services;

use App\Services\Interfaces\UserServiceInterface;
use App\Repositories\UserRepository as UserRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

/**
 * Class UserService
 * @package App\Services
 */
class UserService implements UserServiceInterface
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function paginate($request) {
        $condition = [];
        $condition['keyword'] = addslashes($request->input('keyword'));
        $condition['status'] = $request->input('status');
        $perPage = $request->input('per_page') ?? 10;
        $path = route('admin.user');
        return $this->userRepository->pagination((int)$perPage, ['*'], $condition, [], $path);
    }

    public function create($request) {
        DB::beginTransaction();
        try{
            $payload = $request->except(['_token','send','re_password']);
            if($payload['birthday'] != null){
                $payload['birthday'] = $this->convertBirthdayDate($payload['birthday']);
            }
            $payload['password'] = Hash::make($payload['password']);
            $this->userRepository->create($payload);
            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function update($request, $id) {
        DB::beginTransaction();
        try{
            $payload = $request->except(['_token','send']);
            if($payload['birthday'] != null){
                $payload['birthday'] = $this->convertBirthdayDate($payload['birthday']);
            }
            $this->userRepository->update($id, $payload);
            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function delete($id) {
        DB::beginTransaction();
        try{
            $this->userRepository->delete($id);
            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }

    private function convertBirthdayDate($birthday = ''){
        $birthday = Carbon::createFromFormat('m/d/Y', $birthday)->format('Y-m-d');
        return $birthday;
    }

    public function changeStatus($post = []) {

        DB::beginTransaction();
        try{
            $payload[$post['field']] = $post['status'];
            $id = $post['id'] ?? '';
            $this->userRepository->update($id, $payload);

            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }

    public function changeStatusAll($post = []) {
        DB::beginTransaction();
        try{
            $payload[$post['field']] = $post['status'];
            $ids = $post['ids'] ?? [];
            $this->userRepository->updateByWhereIn('id', $ids, $payload);

            DB::commit();
            return true;

        }catch(\Exception $e ){
            DB::rollBack();
            Log::error($e->getMessage());
            return false;
        }
    }
}
