<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleStoreRequest;
use Illuminate\Http\Request;
use App\Services\Interfaces\RoleServiceInterface as RoleService;
use App\Repositories\RoleRepository as RoleRepository;

class RoleController extends Controller
{
    protected $roleServices;


    protected $roleRepository;

    public function __construct(
        RoleService $roleServices,
        RoleRepository $roleRepository
    )
    {
       $this->roleServices = $roleServices;
       $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $roles =  $this->roleServices->paginate($request);

        return view('backend.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('backend.role.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoleStoreRequest $request)
    {
        if($this->roleServices->create($request)) {
            return redirect(route('admin.role'))->with('success', 'Them nhom thanh cong');
        } else {
            return redirect(route('admin.role'))->with('error', 'Them nhom khong thanh cong');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $role = $this->roleRepository->findById($id);

        return view('backend.role.edit',compact( 'role'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoleStoreRequest $request, string $id)
    {
        if($this->roleServices->update($request, $id)) {
            return redirect(route('admin.role'))->with('success', 'Cap nhat thanh cong');
        } else {
            return redirect(route('admin.role'))->with('error', 'Cap nhat khong thanh cong');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $response['message'] = 'Xoa khong thanh cong';

        if($this->roleServices->delete($id)) {
            $response['success'] = true;
            $response['message'] = 'Xoa thanh cong';
        }

        return $response;
    }

}
