<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct()
    {

    }

    public function index(){
        if(Auth::check()) {
            return redirect(route('admin.dashboard'));
        }

        return view('backend.auth.login');
    }

    public function login(AuthRequest $request) {
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if(Auth::attempt($credentials)){
            return redirect(route('admin.dashboard'))->with('success', 'Dang nhap thanh cong');
        }

        return redirect(route('admin.login'))->with('error', 'Tai khoan hoac mat khau khong chinh xac');
    }

    public function logout(Request $request) {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect(route('admin.login'));
    }
}
