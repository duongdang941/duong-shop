<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;
use App\Services\Interfaces\UserServiceInterface as UserService;
use App\Repositories\Interfaces\DistrictRepositoryInterface as DistrictRepository;
use App\Repositories\Interfaces\WardRepositoryInterface as WardRepository;
use App\Repositories\Interfaces\ProvinceRepositoryInterface as ProvinceRepository;
use App\Repositories\UserRepository as UserRepository;

class UserController extends Controller
{
    protected $userServices;

    protected $districtRepository;

    protected $provinceRepository;

    protected $wardRepository;

    protected $userRepository;

    public function __construct(
        UserService $userServices,
        DistrictRepository $districtRepository,
        ProvinceRepository $provinceRepository,
        WardRepository $wardRepository,
        UserRepository $userRepository
    )
    {
       $this->userServices = $userServices;
       $this->districtRepository = $districtRepository;
       $this->provinceRepository = $provinceRepository;
       $this->wardRepository = $wardRepository;
       $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $users =  $this->userServices->paginate($request);

        return view('backend.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $location = [
            'provinces' => $this->provinceRepository->all(),
        ];

        return view('backend.user.create',compact('location'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserStoreRequest $request)
    {
        if($this->userServices->create($request)) {
            return redirect(route('admin.user'))->with('success', 'Them thanh vien thanh cong');
        } else {
            return redirect(route('admin.user'))->with('error', 'Them thanh vien khong thanh cong');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = $this->userRepository->findById($id);

        $location = [
            'provinces' => $this->provinceRepository->all(),
        ];
        return view('backend.user.edit',compact('location', 'user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserUpdateRequest $request, string $id)
    {
        if($this->userServices->update($request, $id)) {
            return redirect(route('admin.user'))->with('success', 'Cap nhat thanh cong');
        } else {
            return redirect(route('admin.user'))->with('error', 'Cap nhat khong thanh cong');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $response['message'] = 'Xoa khong thanh cong';

        if($this->userServices->delete($id)) {
            $response['success'] = true;
            $response['message'] = 'Xoa thanh cong';
        }

        return $response;
    }

}
