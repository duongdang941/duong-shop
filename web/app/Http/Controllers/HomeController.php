<?php

namespace App\Http\Controllers;

use App\Models\Member;
use App\Models\Team;
use Illuminate\Http\Request;

class HomeController extends Controller
{
   public function index() {
       $member = Member::find(1);
        $teams = $member->teams;
       return $teams;
   }
}
