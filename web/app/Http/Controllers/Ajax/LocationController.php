<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Repositories\WardRepository;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\DistrictRepositoryInterface as DistrictRepository;
use App\Repositories\Interfaces\ProvinceRepositoryInterface as ProvinceRepository;
use App\Repositories\Interfaces\WardRepositoryInterface as WardsRepository;

class LocationController extends Controller
{
    protected $provinceRepository;

    protected $districtRepository;

    protected $wardRepository;

    public function __construct(
        ProvinceRepository $provinceRepository,
        DistrictRepository $districtRepository,
        WardRepository $wardRepository,
    )
    {
        $this->provinceRepository = $provinceRepository;
        $this->districtRepository = $districtRepository;
        $this->wardRepository = $wardRepository;
    }

    public function getDistricts(Request $request) {
        $districts = [];
        $provinceId = $request->input('province_id');
        $province = $this->provinceRepository->findById($provinceId);
        $districts = $province->districts;
        $default = '[Chon Quan/Huyen]';
        $response = [
            'html' => $this->renderHtml($districts, $default)
        ];

        return response()->json($response);
    }

    public function getWards(Request $request) {
        $wards = [];
        $districtId = $request->input('district_id');
        $districts = $this->districtRepository->findById($districtId);
        $wards = $districts->wards;
        $default = '[Chon Phuong/Xa]';
        $response = [
            'html' => $this->renderHtml($wards, $default)
        ];

        return response()->json($response);
    }

    public function renderHtml($data, $default) {
        $html = '<option value="0">' . $default . '</option>';
        foreach ($data as $item) {
            $html .=  '<option value="' . $item->code . '">' . $item->name . '</option>';
        }

        return $html;
    }
}
