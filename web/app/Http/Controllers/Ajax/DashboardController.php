<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;


class DashboardController extends Controller
{
    public function __construct(

    ) {

    }

    public function changeStatus(Request $request) {
        $response['message'] = 'Thay doi trang thai khong thanh cong';
        $post = $request->input();

        $serviceInterfaceNamespace = '\App\Services\\' . ucfirst($post['model']) . 'Service';
        if (class_exists($serviceInterfaceNamespace)) {
            $serviceInstance = app($serviceInterfaceNamespace);

            if($serviceInstance->changeStatus($post)) {
                $response['success'] = true;
                $response['message'] = 'Thay doi trang thai thanh cong';
            }
        }

        return $response;
    }

    public function changeStatusAll(Request $request) {
        $response['message'] = 'Thay doi trang thai khong thanh cong';
        $post = $request->input();

        $serviceInterfaceNamespace = '\App\Services\\' . ucfirst($post['model']) . 'Service';
        if (class_exists($serviceInterfaceNamespace)) {
            $serviceInstance = app($serviceInterfaceNamespace);

            if($serviceInstance->changeStatusAll($post)) {
                $response['success'] = true;
                $response['message'] = 'Thay doi trang thai thanh cong';
            }
        }

        return $response;
    }

}
