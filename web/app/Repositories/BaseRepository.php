<?php

namespace App\Repositories;

use App\Repositories\Interfaces\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $payload = []){
        $model = $this->model->create($payload);
        return $model->fresh();
    }

    public function update(int $id = 0, array $payload = []){
        $model = $this->findById($id);
        $model->update($payload);
        return $model->fresh();
    }

    public function updateByWhereIn(string $whereInField = '', array $whereIn = [], array $payload = []){
        return $this->model->whereIn($whereInField, $whereIn)->update($payload);
    }

    public function delete(int $id = 0) {
        return  $this->findById($id)->delete();
    }

    public function forceDelete(int $id = 0) {
        return $this->findById($id)->forceDelete();
    }

    public function all() {
        return $this->model->all();
    }

    public function pagination(
        int $perPage,
        array $column = ['*'],
        array $condition = [],
        array $join = [],
        string $path = '') {

        $query = $this->model->select($column)->where(function ($query) use ($condition) {
            if(!empty($condition) && isset($condition['keyword'])) {
                $query->where('name', 'LIKE', '%' . $condition['keyword'] . '%');
            }
        });

        if(!empty($join)) {
            $query->join($join);
        }

        return $query->paginate($perPage)
                    ->withQueryString()
                    ->withPath($path);
    }

    public function findById(int $id, array $column = ['*'], array $relation = []) {
        return $this->model->select($column)->with($relation)->findOrFail($id);
    }
}

