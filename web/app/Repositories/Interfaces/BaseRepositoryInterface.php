<?php

namespace App\Repositories\Interfaces;

interface BaseRepositoryInterface
{
    public function create(array $payload = []);

    public function update(int $id = 0, array $payload = []);

    public function updateByWhereIn(string $whereInField = '', array $whereIn = [], array $payload = []);

    public function delete(int $id = 0);

    public function forceDelete(int $id = 0);

    public function all();

    public function pagination(int $perPage, array $column = ['*'], array $condition = [], array $join = [], string $path = '');

    public function findById(int $id, array $column, array $relation);
}
