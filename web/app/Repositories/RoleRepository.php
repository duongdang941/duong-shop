<?php

namespace App\Repositories;

use App\Models\Role;
use App\Services\Interfaces\RoleServiceInterface;

/**
 * Class UserService
 * @package App\Services
 */
class RoleRepository extends BaseRepository implements RoleServiceInterface
{
    protected $model;

    public function __construct(
        Role $model
    ){
        $this->model = $model;
    }

    public function pagination(
        int $perPage,
        array $column = ['*'],
        array $condition = [],
        array $join = [],
        string $path = '') {

        $query = $this->model->select($column)->where(function($query) use ($condition){
            if(!empty($condition)) {
                    if(isset($condition['keyword']) && !empty($condition['keyword'])){
                        $query->where('name', 'LIKE', '%'.$condition['keyword'].'%');
                }

//                if(isset($condition['status']) && $condition['status'] != 0){
//                    $query->where('status', '=', $condition['status']);
//                }
            }

            return $query;
        });

        if(!empty($join)) {
            $query->join(...$join);
        }

        return $query->paginate($perPage)
            ->withQueryString()
            ->withPath($path);
    }

}

