setup:
	@make build
	@make up
	@make stop
	@make composer-update
	@make data
	@make clear
build:
	sudo docker-compose build --no-cache --force-rm
stop:
	docker-compose stop
up:
	docker-compose up -d
composer-update:
	docker exec php bash -c "composer update"
data:
	docker-compose exec php bash -c "php artisan migrate"
	docker-compose exec php bash -c "php artisan db:seed"
clear:
	docker-compose exec php bash -c "php artisan cache:clear"
	docker-compose exec php bash -c "php artisan config:clear"